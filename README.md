Automation Testing Website Tiket.com menggunakan codeceptjs

pada project ini saya membuat sample scenario ada 5.
- Scenario penerbangan
-- pada scenario ini kit akan memilih dari mana kita akan terbang dan kemana tujuan kita, beserta dengan tanggalnya
- Scenario Hotel
-- pada scenario ini kita akan memilih jotel beserta dengan tanggalnya maka list hotel akan muncul
- Scenario Kereta Api
-- pada scenario kita akan memilih berangkat dari bandung ke tujuan berikutnya adalah jaakrta maka akan muncul list kereta apai dari bandung ke jakarta
- Scenario Sewa Mobil
-- pada scenario ini kita akan melakukan sewa mobil pada daerah dki jakarta dan setelah tekan submit, akan keluar list mobil beeserta harganya
- Scenario Hiburan
-- pada scenario ini kita akan memilih jenis hiburan yaitu atraksi yang ada di daerah jakarta, setelah tekan submit maka akan muncul listnya


mengapa saya menggunakan menggunakan codeceptjs?
- karena menggunakan bahasa javascript yang mudah dimengerti
- konsep codeceptjs yang mudah dimengerti
- tidak memakan memory(ram) yang terlalu besar
- bisa terintegrasi ke web driver,testcafe

untuk menjalankan testcasenya cukup ketik :

    yarn start

Semoga ini bermamfaat.

Terima kasih
