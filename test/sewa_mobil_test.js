Feature('Sewa obil');

const url = "https://www.tiket.com/";


Scenario('test something', (I) => {
    I.amOnPage(url);
    //pilih sewa mobil
    I.click('.product-list-content .product-box:nth-child(4)');
    I.wait(5);
    //pilih kota
     I.executeScript(function () {
        var name = "DKI Jakarta";
        var ids = document.querySelectorAll('.jstarget-all-dropdownlocation .listing a');
        var names = document.querySelectorAll('.jstarget-all-dropdownlocation .listing .regional-name');

        for (var i = 0; i < ids.length; i += 1) {
            var  region = {
				id: ids[i].getAttribute('data-value'),
                name: names[i].innerHTML
			}

            if (region.name.toLowerCase().includes(name.toLowerCase()) > 0) {
                document.querySelector('#search-regionalarea').value = region.name;
                document.querySelector('#regionalarea').value = region.id;
                break;
            }
        }
        // now we are inside browser context
    });
    I.wait(5);
    //klik submit
    I.click('.js-check-date .button-primary');
    I.wait(20);
});