Feature('Entertainment');

const url = "https://www.tiket.com/";

  
Scenario('test something', async(I) => {
  I.amOnPage(url);
  //hotel
  I.click('.product-list-content .product-box:nth-child(5)');
  I.wait(10);
  //klik munculkan dropdown;
  I.click('.search__category .search__category--selected .tixicon-down');
  I.wait(5);
  //pilih menu dropdown = kategori
  I.click('.search__category__list .search__category__item:nth-child(3)');
  I.wait(10);
  //field indonesia
  I.fillField('#submit_search span input[type=text]','jakarta');
  I.wait(5);
  //klik submit
  I.click('.btn.btn-primary');
  I.wait(10);
});