Feature('Ticket flight');

const url = "https://www.tiket.com/";

Scenario('test something', (I) => {
  I.amOnPage(url);
  I.click('.product-list-content .product-box:nth-child(1)');
  I.fillField('.product-form-container:nth-child(2) .form-to-input-group:nth-child(1) .widget-input-container input[class=product-search-input]', 'DJB');
  I.click('.product-form-container:nth-child(2) .form-to-input-group:nth-child(1) .widget-drop-down .widget-drop-down-content .flight-search-airport-city #fromDropDownList .ReactVirtualized__Grid__innerScrollContainer #fromDropDownList-airport1');
  I.wait(5);
  I.fillField('.product-form-container:nth-child(2) .form-to-input-group:nth-child(2) .widget-input-container .product-search-input', 'CGK');
  I.click('.product-form-container:nth-child(2) .form-to-input-group:nth-child(1) .widget-drop-down .widget-drop-down-content .flight-search-airport-city #fromDropDownList .ReactVirtualized__Grid__innerScrollContainer #fromDropDownList-airport1');
  I.wait(5);
  I.click('.product-form-container:nth-child(2) .widget-date-input-group-container .widget-drop-down .widget-drop-down-content .widget-date-picker .widget-date-picker-content .DayPicker .DayPicker_focusRegion .DayPicker_transitionContainer .CalendarMonthGrid .CalendarMonthGrid_month__horizontal .CalendarMonth .CalendarMonth_table tbody tr:nth-child(4) .CalendarDay:nth-child(2) .widget-date-picker-day');
  I.wait(5);
  I.click('.product-form .product-form-container:nth-child(2) .widget-date-input-group-container .input-date-type:nth-child(2) .widget-input-container .product-search-input-label .check.v3 #productSearchReturnCheckbox');
  I.wait(5);


  //I.fillField('.product-form .product-form-container:nth-child(2) .widget-date-input-group-container .input-date-type:nth-child(2) .widget-input-container .product-search-input-container .product-search-input','Sen, 19 Agt 2019');
  //I.click('.product-form .product-form-container:nth-child(2) .widget-date-input-group-container .input-date-type:nth-child(2) .widget-drop-down .widget-drop-down-content .widget-date-picker .widget-date-picker-content .DayPicker .DayPicker_focusRegion .DayPicker_transitionContainer .CalendarMonthGrid .CalendarMonthGrid_month__horizontal:nth-child(3) .CalendarMonth .CalendarMonth_table tbody tr:nth-child(3) .CalendarDay:nth-child(4)');
  //I.click('.product-form .product-form-container:nth-child(2) .widget-date-input-group-container .input-date-type:nth-child(2) .widget-drop-down .widget-drop-down-content .widget-date-picker .widget-date-picker-content .DayPicker .DayPicker_focusRegion .DayPicker_transitionContainer .CalendarMonthGrid .CalendarMonthGrid_month__horizontal:nth-child(3) .CalendarMonth .CalendarMonth_table tbody tr:nth-child(3) .CalendarDay:nth-child(2)');
  //I.wait(5);
  I.click('#passengerCabin .widget-drop-down .widget-drop-down-content .passenger-cabin-drop-down-container .passenger-cabin-drop-down-footer .passenger-cabin-drop-down-text span');
  I.wait(5);
  I.click('#productWidget .product-form .product-form-container:nth-child(3) button.product-form-search-btn');
  I.wait(20);
});
