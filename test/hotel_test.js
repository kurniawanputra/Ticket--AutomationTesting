Feature('Ticket flight');

const url = "https://www.tiket.com/";

const AmbilTanggal = function getDate(date, month, year) {
    var days = 'Min,Sen,Sel,Rab,Kam,Jum,Sab'.split(',');
    var months = 'Jan,Feb,Mar,Apr,Mei,Jun,Jul,Agt,Sep,Okt,Nov,Des'.split(',');
    var objDate = new Date([year, month, date].join('/'));
    
    return [
      days[objDate.getDay()] + ',',
      objDate.getDate(),
      months[objDate.getMonth()],
      objDate.getFullYear()
    ].join(' ');      
  }

  
Scenario('test something', async(I) => {
  I.amOnPage(url);
  //hotel
  I.click('.product-list-content .product-box:nth-child(2)');
  I.wait(5);
  //ketik jakarta 
  I.fillField('#productSearchDestination','jakarta');
  I.wait(5);
  //pilih jakarta
  I.click('#destinationDropDownList .ReactVirtualized__Grid__innerScrollContainer #destinationDropDownList-place1');
  I.wait(5);
  //klik check in
  I.click('#startDate');
  I.wait(5);
  //pilih tanggal Check in
  I.executeScript(function() {
    // now we are inside browser context
    document.querySelector('#productSearchCheckIn').value="Rab, 25 Sep 2019";
  });
  I.wait(5);
  //pilih tanggal Check Out
  I.executeScript(function() {
    // now we are inside browser context
    document.querySelector('#productSearchCheckOut').value="Sab, 28 Sep 2019";
  });
  I.wait(5);
  //klik close date
  I.click('.widget-drop-down-close-btn');
  I.wait(5);
  //close kamar & tamu
  I.click('.product-form-container.product-form-search-group .widget-hotel-guest-room .widget-drop-down .widget-drop-down-content .widget-drop-down-close-btn');
  I.wait(3);
  //klik search
  I.click('.product-form-container.product-form-button-group button.product-form-search-btn');
  I.wait(15);
});